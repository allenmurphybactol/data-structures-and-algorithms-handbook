# Aspiring Game Designer

## Allen Murphy Bactol

👋 Aspiring Game Designer 🎮🕹 — 💌 allenmurphybactol@student.laverdad.edu.ph — Apalit, Pampanga

![alt bsis_2_bactol_allenmurphy3.jpg](images/bsis_2_bactol_allenmurphy.jpg)

### Bio

**Good to know:** ...that, I'm alive.

**Motto:** ['ASMR: Art Speaks More Resonance.', 'Inspiration begets the motivation for innovation.']

**Languages:** Python, Java, SQL

**Other Technologies:** GameMaker, Google Workspace, Medibang

**Personality Type:** [Turbulent Logician (INTP-T)](https://www.16personalities.com/profiles/d471cce65a8c6)

<!-- END -->